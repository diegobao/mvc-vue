﻿using System.Web.Mvc;

namespace MVCVue.Areas.dss
{
    public class dssAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "dss";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "dss_default",
                "dss/{controller}/{action}/{id}",
                new { controller = "Home2", action = "Index", id = UrlParameter.Optional }
            );

            //context.MapRoute(
            //    name: "dss_vue",
            //    url: "{*url}",
            //    defaults: new { controller = "Home2", action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}