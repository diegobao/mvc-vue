﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCVue.Areas.dss.Controllers
{
    public class DataController : Controller
    {
        // GET: dss/Data
        public ActionResult Index()
        {
            return Json(new { text = "Hello" }, JsonRequestBehavior.AllowGet);
        }
    }
}